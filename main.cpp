#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <iostream>
#include <vector>
#include <string>
#include <fstream>


bool checkWhitePixel(cv::Vec3b& pixel) {
    if(pixel[0] == 255 && pixel[1] == 255 && pixel[2] == 255) {
        return true;
    } else {

        return false;
    }
}

bool checkColorPixel(cv::Vec3b& pixel, int rVal, int gVal, int bVal) {
    if(pixel[0] == bVal && pixel[1] == gVal && pixel[2] == rVal) {
        return true;
    } else {
        return false;
    }

}







int calculateLength(cv::Mat& img, int r, int g, int b) {
    CV_Assert(img.depth() != sizeof(uchar));
    cv::Mat  res(img.rows, img.cols, CV_8UC3);
    cv::Mat_<cv::Vec3b> _I = img;
    int length = 0;
    for(int i = 1; i < _I.rows-1; i++) {
        for(int j = 1; j < _I.cols-1; j++) {
            bool color = checkColorPixel(_I(i, j), r, g, b);
            bool upW = checkWhitePixel(_I(i, j+1));
            bool downW = checkWhitePixel(_I(i, j-1));
            bool leftW = checkWhitePixel(_I(i+1, j));
            bool rightW = checkWhitePixel(_I(i-1, j));
            if(color && (upW || downW || leftW || rightW)) {
                length++;
            }
        }
    }
    return length;
}

double malinowskaFactor(int field, int length) {

    return (length / (2 * sqrt(3.14 * field)) - 1);
}

int calculateField(cv::Mat& img, int r, int g, int b) {
    CV_Assert(img.depth() != sizeof(uchar));
    cv::Mat  res(img.rows, img.cols, CV_8UC3);
    cv::Mat_<cv::Vec3b> _I = img;
    int field = 0;

    for(int i = 0; i < _I.rows; i++) {
        for(int j = 0; j < _I.cols; j++) {
            if(checkColorPixel(_I(i, j), r, g, b)) //black pixel check
                field++;
        }
    }
    return field;
}

long calculateSmallM(int p, int q, cv::Mat& img, int r, int g, int b) {
    CV_Assert(img.depth() != sizeof(uchar));
    cv::Mat  res(img.rows, img.cols, CV_8UC3);
    cv::Mat_<cv::Vec3b> _I = img;
    long m = 0;

    for(int i = 0; i < _I.rows; i++) {
        for(int j = 0; j < _I.cols; j++) {
            if(checkColorPixel(_I(i, j), r, g, b)) //black pixel check
                m += pow(i, p) * pow(j, q) * 1;
        }
    }
    return m;

}

int calculateAngle(cv::Mat& img, int r, int g, int b) {
    CV_Assert(img.depth() != sizeof(uchar));
    cv::Mat  res(img.rows, img.cols, CV_8UC3);
    cv::Mat_<cv::Vec3b> _I = img;

    int maxI = 0;
    int minI = img.rows;
    int maxJ = 0;
    int minJ = img.cols;

    for(int i = 0; i < _I.rows; i++) {
        for(int j = 0; j < _I.cols; j++) {
            if(checkColorPixel(_I(i, j), r, g, b)) {
                if(i > maxI)
                    maxI = i;
                if(i < minI)
                    minI = i;
                if(j > maxJ)
                    maxJ = j;
                if(j < minJ)
                    minJ = j;
            }

        }
    }

    int x = (maxI + minI) / 2;
    int y = (maxJ + minJ) / 2;


    int i = calculateSmallM(1, 0, _I, r, g, b) / calculateSmallM(0, 0, _I, r, g, b);
    int j = calculateSmallM(0, 1, _I, r, g, b) / calculateSmallM(0, 0, _I, r, g, b);

    int angle = -atan2(y - j, x - i) * 180/3.14;

    return angle;
}

std::string task1(cv::Mat& file, std::string name) {
    int length = calculateLength(file, 0, 0, 0);
    int field = calculateField(file, 0, 0, 0);
    double malFac = malinowskaFactor(field, length);
    long m_0_0 = calculateSmallM(0, 0, file, 0, 0, 0);
    long m_0_1 = calculateSmallM(0, 1, file, 0, 0, 0);
    long m_1_0 = calculateSmallM(1, 0, file, 0, 0, 0);
    long m_1_1 = calculateSmallM(1, 1, file, 0, 0, 0);
    long m_2_0 = calculateSmallM(2, 0, file, 0, 0, 0);
    long m_0_2 = calculateSmallM(0, 2, file, 0, 0, 0);
    double M_2_0 = m_2_0 - pow(m_1_0, 2) / m_0_0;
    double M_0_2 = m_0_2 - pow(m_0_1, 2) / m_0_0;
    double M1 = (M_2_0 + M_0_2) / pow(m_0_0,2);
    double M_1_1 = m_1_1 - (m_1_0 * m_0_1) / m_0_0;
    long double powtest = pow(M_1_1, 2);
    long double M20M02 = M_0_2 * M_2_0 - pow(M_1_1, 2);
    double M7 = (M_2_0 * M_0_2 - pow(M_1_1,2))/pow(m_0_0, 4);

    return "File: " + name + " S: " + std::to_string(field) + " L: " + std::to_string(length) + " W3: " + std::to_string(malFac) + " M1: " + std::to_string(M1) + " M7: " + std::to_string(M7) + "\n";
}

std::string task2(cv::Mat& file, std::string name) {
    std::string output = "";
    int g = 180;
    for(int r=0; r<=180; r+=45) {
        int l = calculateLength(file, r, g, 0);
        int s = calculateField(file, r, g, 0);
        double W3 = malinowskaFactor(s, l);
        long m_0_0 = calculateSmallM(0, 0, file, r, g, 0);
        long m_0_1 = calculateSmallM(0, 1, file, r, g, 0);
        long m_1_0 = calculateSmallM(1, 0, file, r, g, 0);
        long m_1_1 = calculateSmallM(1, 1, file, r, g, 0);
        long m_2_0 = calculateSmallM(2, 0, file, r, g, 0);
        long m_0_2 = calculateSmallM(0, 2, file, r, g, 0);
        double M_2_0 = m_2_0 - pow(m_1_0, 2) / m_0_0;
        double M_0_2 = m_0_2 - pow(m_0_1, 2) / m_0_0;
        double M1 = (M_2_0 + M_0_2) / pow(m_0_0,2);
        double M_1_1 = m_1_1 - (m_1_0 * m_0_1) / m_0_0;
        double M7 = (M_2_0 * M_0_2 - pow(M_1_1,2))/pow(m_0_0, 4);
        int angle = calculateAngle(file, r, g, 0);
        output += "Srzalka R: " + std::to_string(r) + " Angle: " + std::to_string(angle) + " S: " + std::to_string(s) + " L: " + std::to_string(l) + " W3: " + std::to_string(W3) + " M1: " + std::to_string(M1) + " M7 " + std::to_string(M7) + "\n";

        g -= 45;
    }
    return output;
}

int main(int, char* []) {
    std::ofstream stream;
    stream.open("/Users/orzelke/CLionProjects/Laboratory3/output.txt");
    cv::Mat image = cv::imread("/Users/orzelke/CLionProjects/Laboratory3/elipsa.dib");
    stream << task1(image, "elipsa");
    cv::Mat image2 = cv::imread("/Users/orzelke/CLionProjects/Laboratory3/elipsa1.dib");
    stream << task1(image2, "elipsa1");
    cv::Mat image3 = cv::imread("/Users/orzelke/CLionProjects/Laboratory3/kolo.dib");
    stream << task1(image3, "kolo");
    cv::Mat image4 = cv::imread("/Users/orzelke/CLionProjects/Laboratory3/prost.dib");
    stream << task1(image4, "prost");
    cv::Mat image5 = cv::imread("/Users/orzelke/CLionProjects/Laboratory3/troj.dib");
    stream << task1(image5, "troj");
    cv::Mat image6 = cv::imread("/Users/orzelke/CLionProjects/Laboratory3/strzalki_1.dib");
    stream << task2(image6, "strzalki_1.dib");
    cv::Mat image7 = cv::imread("/Users/orzelke/CLionProjects/Laboratory3/strzalki_2.dib");
    stream << task2(image7, "strzalki_2.dib");
    stream.close();



    return 0;
}
